*** Settings ***
Suite Setup
Library           SeleniumLibrary
Library           BuiltIn

*** Variables ***
@{EnterUsername}    soumyar@pragiti.com    ritesh2@gmail.com    ritesh@pragiti.com    soumya@yopmail.com
@{RegistrationInvalidusers}    1234#$&^
@{validRegistration}    soumyar@pragiti.com    Ritesh    Nayak    (201) 334-5333    riteshg@pragiti.com
${answer}         42
@{subscription}    aci@c.com    sgsg@gmail.com
@{Address}        pragiti    elena    paul    9w 57th street    New York    US-NY    10019
...               2015551234    Pragiti    vikram    rao    9w 57th street    New York    US-NY
...               10019    2013743223    ABCD    dheeraj    rane    9w 57th street    New York
...               US-NY    10019    2015113114    xyz    sanket    g    2322 Old House Drive
...               Washington    US-OH    43085    2015113114
@{product Image}    /medias/mp-00002867-media-65Wx65H?context=bWFzdGVyfHJvb3R8MTQxNHxpbWFnZS9qcGVnfGg1OS9oZTkvODc5OTM2NzQzMDE3NC9tcC0wMDAwMjg2N19tZWRpYV82NVd4NjVIfGNkMGI3YjgyOGQzYzdjMmE1ZjliNmZjNWNmMWMzNTJjZDhkNmZkMTM1YjNkMWJjOWIzOTA1OGU1MmQ5OWMzM2Y
@{CurrentAndNewPWD}    12321234    12341234    12341234    12341234
@{ZipCode}        29406    .....    82941
@{status}
@{statusa}
@{card}           411111111111111111111111111111111111111111111111111111111111111111111111111111111111
@{Email}          souar@pragiti.com    soumya@yopmail.com

*** Test Cases ***
Login
    [Setup]    openbrowser    https://accstorefront.c4f4c56-foodservi1-s1-public.model-t.cc.commerce.ondemand.com/en_US/    chrome
    Maximize Browser Window
    Sleep    5s
    Click Element    id=signin-popup    \    #Login pop up window is displayed
    Sleep    5s
    Element Text Should Be    class=box-message__title    Login to FSX    #verfiy text Login to FSX
    Element Text Should Be    xpath=//h3[contains(text(),'New Customer?')]    New Customer?Register Here    #verify text New Customer?Register Here
    Click Element    class=create-account-link    \    #click on register here link
    Sleep    5s
    Click Element    id=signin-popup    \    #Login pop up window is displayed
    Element Text Should Be    xpath=(//*[@for="j_username"])[2]    Username/Email Address    #Username/Email Address
    Click Element    xpath=//form[@id='loginajaxform']//input[@name='j_username']    \    #clicks on username field
    Input Text    xpath=//form[@id='loginajaxform']//input[@name='j_username']    @{EnterUsername}[1]
    Click Element    id=j_password
    Input Text    id=j_password    32424231
    Sleep    3s
    Click Element    id=loginsubmit    \    #clicks login button
    Sleep    5s
    Element Text Should Be    class=error    Username/Email address or password are incorrect.    #verify validation message.
    Click Element    id=j_password    \    #clicks inside password field
    Input Text    id=j_password    12341234    #enters password in password field
    Input Text    id=j_password    1234123    #enters password in password field
    Sleep    10s
    Element Text Should Be    xpath=//label[text()='Please enter at least 8 characters.']    Please enter at least 8 characters.    #Verifies error message
    Sleep    5s
    #Element Text Should Be    xpath=//div[@class='form-group has-error']//input[@id='j_password']    password    #verifies password is bullet
    Element Text Should Be    class=forgot-password-link    Forgot Password?    #verifies forgot password link
    Sleep    5s
    Page Should Contain Button    id=loginsubmit    Login    #verifies login button available on page.
    Sleep    5s
    Clear Element Text    xpath=//form[@id='loginajaxform']//input[@name='j_username']
    Sleep    2s
    Clear Element Text    id=j_password
    Sleep    2s
    Click Button    id=loginsubmit
    Sleep    2s
    Element Text Should Be    class=form-error    This is a required field.
    Sleep    5s
    Click Element    xpath=//form[@id='loginajaxform']//input[@name='j_username']
    Input Text    xpath=//form[@id='loginajaxform']//input[@name='j_username']    hjhsdkjk@pragiti.com
    Click Element    id=j_password    \    #verifies user is unable to login with invalid credentials.
    Input Text    id=j_password    8782838726
    Sleep    3s
    Click Element    id=loginsubmit
    Sleep    3s
    Click Element    xpath=//form[@id='loginajaxform']//input[@name='j_username']
    Input Text    xpath=//form[@id='loginajaxform']//input[@name='j_username']    @{EnterUsername}[0]
    Click Element    id=j_password
    Input Text    id=j_password    12341234
    Sleep    3s
    Click Element    id=loginsubmit
    Sleep    3s
    Close Browser

Account Registration
    [Setup]    openbrowser    https://accstorefront.c4f4c56-foodservi1-s1-public.model-t.cc.commerce.ondemand.com/fsxstorefront/en_US/    chrome
    Maximize Browser Window
    Click Element    id=signin-popup
    Sleep    2s
    Click Element    class=create-account-link    #navigates to login page
    sleep    2s
    Element Text Should Be    xpath=//h2[contains(text(),'Sign In')]    Sign In
    Sleep    2s
    Element Text Should Be    xpath=//h2[contains(text(),'Register for a new account')]    Register for a new account
    Sleep    2s
    Element Text Should Be    xpath=//label[@for='firstName']    First Name
    Element Text Should Be    xpath=//label[@for='lastName']    Last Name
    Element Text Should Be    xpath=//label[@for='phoneFull']    Phone Number
    Element Text Should Be    xpath=//label[@for='companyName']    Company Name
    Element Text Should Be    xpath=//label[@for='industry']    Industry
    Element Text Should Be    xpath=//label[@for='email']    Email Address
    Element Text Should Be    xpath=//span[@class='text-muted pull-right']    (Minimum 8 characters)
    Sleep    5s
    Element Text Should Be    xpath=(//label[@for='password'])[2]    Password
    Element Text Should Be    xpath=//label[@for='confirmPwd']    Confirm Password
    Sleep    2s
    Click Button    xpath=//button[@class='btn btn-primary register-details-btn']
    Sleep    3s
    Click Element    xpath=//label[@for='firstName']
    Input Text    id=firstName    @{RegistrationInvalidusers}
    Click Element    xpath=//div[@class='contentwrapper innerpage']
    Sleep    5s
    Element Text Should Be    id=firstName-error    Please enter a valid First Name
    Sleep    5s
    Clear Element Text    id=firstName
    Input Text    id=firstName    @{validRegistration}[1]
    Sleep    3s
    Input Text    id=lastName    @{RegistrationInvalidusers}
    Click Element    xpath=//div[@class='contentwrapper innerpage']
    Element Text Should Be    id=lastName-error    Please enter a valid Last Name
    Sleep    5s
    Clear Element Text    id=lastName
    Input Text    id=lastName    @{validRegistration}[2]
    Sleep    3s
    Click Element    xpath=//div[contains(@class,'iti-arrow')]
    Sleep    10s
    countryphoneno
    Click Element    id=phoneNumber
    Input Text    id=phoneNumber    2345
    Click Element    xpath=//div[@class='contentwrapper innerpage']
    Element Text Should Be    id=phoneNumber-error    Please enter a valid phone number.
    Sleep    5s
    Clear Element Text    id=phoneNumber
    Input Text    id=phoneNumber    @{validRegistration}[3]
    Click Element    xpath=//input[@id='companyName']
    Input Text    xpath=//input[@id='companyName']    testcompany
    Sleep    3s
    Click Element    id=industry
    Element Text Should Be    xpath=//option[contains(text(),'Fine Dining')]    Fine Dining
    Sleep    5s
    Select From List By Value    id=industry    Fine_Dining
    Click Element    id=email
    Input Text    id=email    qwert
    Click Element    xpath=//div[@class='contentwrapper innerpage']
    Element Text Should Be    id=email-error    Please enter a valid email address.
    Sleep    5s
    Input Text    id=email    @{validRegistration}[0]
    Click Element    id=password
    Input Text    id=password    12121
    Click Element    xpath=//div[@class='contentwrapper innerpage']
    Element Text Should Be    id=password-error    Minimum password length should be 8 characters.
    Sleep    5s
    Clear Element Text    id=password
    Input Text    id=password    1234 234
    Clear Element Text    id=password
    Input Text    id=password    12341234
    Click Element    id=confirmPwd
    Input Text    id=confirmPwd    1234123456
    Click Element    xpath=//div[@class='contentwrapper innerpage']
    Element Text Should Be    id=confirmPwd-error    Password and Confirm Password do not match.
    Clear Element Text    id=confirmPwd
    Input Text    id=confirmPwd    12341234
    Sleep    10s
    Click Element    xpath=//button[@class='btn btn-primary register-details-btn']    #clicks on registration button
    Sleep    5s
    Element Text Should Be    id=email.errors    An account already exists for this email address.
    Sleep    5s
    Click Element    id=email
    Input Text    id=email    @{validRegistration}[4]
    Click Element    id=password
    Input Text    id=password    12341234
    Sleep    5s
    Click Element    id=confirmPwd
    Input Text    id=confirmPwd    12341234
    Sleep    5s
    Click Element    xpath=//button[@class='btn btn-primary register-details-btn']    #clicks on registration button
    Sleep    5s
    ${Sellerdetail}    Get Text    xpath=//div[@class='alert alert-info alert-dismissable getAccAlert']    #verifies success message
    Sleep    3s
    Close Browser

Homepage
    [Setup]    openbrowser    https://accstorefront.c4f4c56-foodservi1-s1-public.model-t.cc.commerce.ondemand.com/en_US/    chrome
    Maximize Browser Window
    #Unauntheticated user
    Sleep    10s
    Element Text Should Be    id=signin-popup    Login    #verfiy text Login to FSX
    Sleep    5s
    Element Should Contain    xpath=//li[@class='mywishlist account-links__last']    Wishlist    #verify text wishlist in header for aunthenticated user.
    Sleep    2s
    Click Element    xpath=//li[@class='mywishlist account-links__last']    \    #verifies aunthenticated user is able to click wishlist link
    Click Element    id=signin-popup    \    #Login pop up window is displayed
    Click Element    xpath=//form[@id='loginajaxform']//input[@name='j_username']    \    #clicks on username field
    Input Text    xpath=//form[@id='loginajaxform']//input[@name='j_username']    @{EnterUsername}[0]
    Click Element    id=j_password
    Input Text    id=j_password    12341234
    Sleep    3s
    Click Element    id=loginsubmit    \    #clicks login button
    Sleep    2s
    Click Element    xpath=//div[contains(@class,'yCmsComponent col-xs-5 col-sm-4 col-md-3 navbar-header__logo')]//img[@class='img-responsive']
    Sleep    2s
    Element Text Should Be    xpath=//li[@class='dropdown mywishlist js-mylistdata account-links__last']    Wishlist    #verify text wishlist in header
    Sleep    5s
    Click Element    xpath=//li[@class='dropdown mywishlist js-mylistdata account-links__last']    #verifies user is able to click wishlist link
    Sleep    2s
    Click Element    xpath=//div[@class='dropdown-menu mywishlist_options']//a[@class='dropdown-menu-btnclose'][contains(text(),'X')]
    Sleep    2s
    ${Sellerdetail}    Get Text    id=myaccount-popup
    Sleep    5s
    Element Text Should Be    xpath=//a[@id='myaccount-popup']    My Account    #verifies text My Account on the header
    Sleep    2s
    Click Element    xpath=//a[@id='myaccount-popup']    #clicks on My Account header
    Sleep    5s
    Click Element    xpath=//div[@class='dropdown-menu my-account']//a[@class='dropdown-menu-btnclose'][contains(text(),'X')]    #closes my acccount popup
    Sleep    2s
    Authenticated user
    Sleep    2s
    Mouse Over    xpath=//a[@title='Kitchen Equipment']
    Sleep    5s
    Click Element    xpath=//a[@title='Kitchen Equipment']    #clicks on header Kitchen Equipment
    Mouse Over    xpath=//a[@title='Light/Counter Equipment']
    Sleep    5s
    Click Element    xpath=//a[@title='Light/Counter Equipment']    #clicks on header Kitchen Equipment
    Mouse Over    xpath=//a[@title='Bar Equipment']
    Sleep    5s
    #Click Element    xpath=//a[@title='Bar Equipment']    #clicks on header Kitchen Equipment
    Sleep    5s
    Mouse Over    xpath=//span[@class='yCmsComponent wrapper__link']//a[contains(text(),'Tabletop')]
    Sleep    5s
    Click Element    xpath=//span[@class='yCmsComponent wrapper__link']//a[contains(text(),'Tabletop')]    #clicks on header Kitchen Equipment
    Mouse Over    xpath=//a[@title='Smallwares']
    Sleep    5s
    Click Element    xpath=//a[@title='Smallwares']    #clicks on header Kitchen Equipment
    Mouse Over    xpath=//a[@title='Furniture']
    Sleep    5s
    Click Element    xpath=//a[@title='Furniture']    #clicks on header Kitchen Equipment
    Mouse Over    xpath=//a[@title='Lodging']
    Sleep    5s
    Click Element    xpath=//a[@title='Lodging']    #clicks on header Kitchen Equipment
    Mouse Over    xpath=//a[@title='Janitorial']
    Sleep    5s
    Click Element    xpath=//a[@title='Janitorial']    #clicks on header Kitchen Equipment
    Sleep    5s
    Mouse Over    xpath=//a[@title='Kitchen Equipment']
    Sleep    5s
    Click Element    xpath=//a[contains(text(),'Cooking Equipment')]
    Sleep    3s
    Mouse Over    xpath=//a[@title='Kitchen Equipment']
    Sleep    5s
    Click Element    xpath=//a[contains(text(),'Food Preparation')]
    Sleep    5s
    Mouse Over    xpath=//a[@title='Kitchen Equipment']
    Sleep    4s
    Mouse over    xpath=//a[@title='Food Preparation']
    Sleep    5s
    Click Element    xpath=(//a[@title='Chopper'])[2]
    #Footer
    Element Text Should Be    xpath=//a[@title='FAQs']    FAQs    Verifies Text FAQ on footer
    Element Text Should Be    xpath=//a[@title='Privacy Policy']    Privacy Policy
    Element Text Should Be    xpath=//a[@title='About Us']    About Us
    Element Text Should Be    xpath=//a[@title='Contact Us']    Contact Us
    Element Text Should Be    xpath=//a[@title='Track Order']    Track Order
    Element Text Should Be    xpath=//a[@title='Shop Now']    Shop Now
    Element Text Should Be    xpath=//a[@title='Product Conditions']    Product Conditions
    Click Element    xpath=//a[@title='FAQs']
    Sleep    2s
    Click Element    xpath=//a[@title='Privacy Policy']
    Sleep    2s
    Click Element    xpath=//a[@title='About Us']
    Sleep    2s
    Click Element    xpath=//a[@title='Contact Us']
    Sleep    2s
    Click Element    xpath=//a[@title='Track Order']
    Sleep    2s
    Click Element    xpath=//a[@title='Shop Now']
    Sleep    2s
    Click Element    xpath=//a[@title='Product Conditions']
    Sleep    2s
    Element Text Should Be    xpath=//div[@class='callus-number']    Email Us: custservice@foodserviceexchange.net
    Sleep    2s
    Element should be visible    xpath=//a[@href='www.facebook.com/fsx']
    Element should be visible    //a[@href='www.linkedin.com/company/fsx/']
    Element should be visible    xpath=//a[@href='www.instagram.com/company/fsx/']
    Element Text Should Be    xpath=//h2[@class='latest-deal-input-title']    Join our family to keep up with latest discount and promotion!
    Element Attribute Value Should Be    xpath=//input[@id='email_newsletter_input-latestdeal']    placeholder    Please enter your email id
    Click Element    xpath=//button[@id='sub-btn-primary-bottom']
    Element Text Should Be    xpath=//div[@class='validation']    Please enter your email address.
    Sleep    2s
    Click Element    xpath=//input[@id='email_newsletter_input-latestdeal']
    Sleep    5s
    Input Text    xpath=//input[@id='email_newsletter_input-latestdeal']    @{subscription}[0]    #new email id
    Sleep    10s
    Click Element    xpath=//button[@id='sub-btn-primary-bottom']
    Sleep    5s
    Element Text Should Be    xpath=//div[@class='validation']    Congratulations! Your mail has been sent successfully.
    Sleep    2s
    Input Text    xpath=//input[@id='email_newsletter_input-latestdeal']    @{subscription}[1]    #repeated email id
    Sleep    10s
    Click Element    xpath=//button[@id='sub-btn-primary-bottom']
    Sleep    5s
    Element Text Should Be    xpath=//div[@class='validation']    Entered email address is already subscribed.

PLP
    [Setup]    openbrowser    https://accstorefront.c4f4c56-foodservi1-s1-public.model-t.cc.commerce.ondemand.com/fsxstorefront/fsx/en_US/    chrome
    Maximize Browser Window
    Click Element    id=signin-popup
    Click Element    xpath=//form[@id='loginajaxform']//input[@name='j_username']
    Input Text    xpath=//form[@id='loginajaxform']//input[@name='j_username']    @{EnterUsername}[0]
    Click Element    id=j_password
    Input Text    id=j_password    12341234
    Sleep    3s
    Click Element    id=loginsubmit
    Sleep    2s
    Click Element    xpath=//a[@title='Kitchen Equipment']    #clicks on header Kitchen Equipment    #Root Header
    Sleep    5s
    #Element Text Should Be    xpath=//h1[text()='Kitchen Equipment']    Kitchen Equipment    #Verifies Breadcrumb title on page
    Sleep    5s
    Element Should Be Enabled    xpath=//a[@id='list']
    Sleep    5s
    Click Element    xpath=//h3[contains(text(),'Category')]    \    #expands category facet
    Sleep    2s
    Click Element    xpath=//h3[contains(text(),'Category')]    \    #Closes category facet
    Sleep    5s
    Click Element    xpath=//h3[contains(text(),'Seller')]    \    #expands Seller facet
    Sleep    2s
    Element Should Be Visible    xpath=//div[contains(@class,'product-filter__block product-filter__collapse opend active')]//ul[contains(@class,'product-filter__block_items')]    \    #verifies filters are available inside facets
    Click Element    xpath=//label[contains(text(),'SAP S1 seller 2')]    #Selects check box
    Sleep    5s
    BuiltIn.Repeat Keyword    1 times    Click Element    xpath=//h3[contains(text(),'Seller')]
    Sleep    5s
    Click Element    //label[contains(text(),'SAP S1 seller 3')]    #Selects check box
    Sleep    5s
    BuiltIn.Repeat Keyword    2 times    Click Element    xpath=//span[contains(@class,'filter-applied__item_remove')]
    Sleep    2s
    Element Should Be Visible    xpath=/html[1]/body[1]/main[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[1]/a[1]    #verifies default grid mode.
    Sleep    2s
    Click Element    class=product-listing__option_view-list    #clicks list view
    Sleep    2s
    Click Element    class=product-listing__option_view-grid    #clicks grid view
    Sleep    2s
    ${Sellerdetail}    Get Text    class=product-listing__option_view-total
    Element Text Should Be    xpath=//label[contains(text(),'Sort By')]    Sort By
    Sleep    2s
    Select From List    id=sortOptions2    Product Name: A to Z
    Sleep    2s
    Select From List    id=sortOptions2    Product Name: Z to A
    Sleep    2s
    Select From List    id=sortOptions2    Price: Low to High
    Sleep    2s
    Select From List    id=sortOptions2    Price: High to Low
    Sleep    2s
    Select From List    id=sortOptions2    Relevance
    Sleep    2s
    Page Should Contain Image    /medias/mp-00000027-media-300Wx300H?context=bWFzdGVyfHJvb3R8NjcxNHxpbWFnZS9qcGVnfGhmMy9oODkvODc5ODc5NTcyNjg3OC9tcC0wMDAwMDAyN19tZWRpYV8zMDBXeDMwMEh8NmU4YjI0N2E5MjRhM2Q0NzNjOWEwYzlmMTE4ZTU0Y2I0NTFkNjNhMDkxYjNiMGI4MTllMzYxMjYxNjA2YjQ2Ng    \    #verfies product image is displayed
    Wait Until Page Contains    DoughXpress DM-18 Manual Pizza Dough Press - 18", 220V    timeout=10    #verifies product title is visible
    Sleep    5s
    Click Element    xpath=/html/body/main/div[1]/div[2]/div/div/div/div[3]/div[2]/div[1]/div[3]/div[1]/div/div[1]/div/a/img    \    #click on product image
    sleep    5s
    Go Back
    Sleep    5s
    Kitchen PLP Compare    #compare functionality
    Sleep    5s
    #Click Element    xpath=//div[11]//div[1]//div[2]//div[1]//h2[1]//a[1]    #click on product title
    Go Back
    Sleep    3s
    Element Should Be Visible    xpath=//div[contains(@class,'col-sm-12 col-md-9 listing-product-image')]//div[2]//div[1]//div[2]//p[1]    \    #verifies product price on plp
    Sleep    2s
    Element Should Be Visible    xpath=//p[text()='mp-00000049']    \    #verifies product number
    Sleep    2s
    Element Should Be Visible    xpath=//div[11]//div[1]//div[2]//p[5]//strong[1]    \    #verifies instock
    Sleep    2s
    Element Should Be Visible    xpath=//div[11]//div[1]//div[2]//div[2]//a[1]    \    #verifies quick button is visible
    Sleep    2s
    Click Element    xpath=//div[11]//div[1]//div[2]//div[2]//a[1]
    Sleep    5s
    Click Element    xpath=//a[@title='Light/Counter Equipment']    #clicks on header Light/Courier Equipment
    Sleep    5s
    #Click Element    xpath=//a[@title='Bar Equipment']    #clicks on header Bar Equipment
    Sleep    5s
    Click Element    xpath=//a[@title='Tabletop']    #clicks on header Tabletop
    Sleep    5s
    Click Element    xpath=//a[@title='Smallwares']    #clicks on header smallwares Equipment
    Sleep    5s
    Click Element    xpath=//a[@title='Furniture']    #clicks on header Furniture Equipment
    Sleep    5s
    Click Element    xpath=//a[@title='Lodging']    #clicks on header Lodging Equipment
    Sleep    5s
    Click Element    xpath=//a[@title='Janitorial']    #clicks on header Janitorial Equipment
    Sleep    5s
    Mouse Over    xpath=//a[@title='Kitchen Equipment']    #Secondary navigation
    Sleep    5s
    Click Element    xpath=//a[@title='Cooking Equipment']
    Sleep    5s
    Element Text Should Be    xpath=//h1[text()='Cooking Equipment']    Cooking Equipment    #Verifies Breadcrumb title on page
    Mouse Over    xpath=//a[@title='Kitchen Equipment']
    Sleep    10s
    Click Element    xpath=//div[@id='mCSB_1_container']//a[contains(text(),'Refrigeration')]
    Sleep    2s
    Element Text Should Be    xpath=//h1[contains(text(),'Refrigeration')]    Refrigeration    #Verifies Breadcrumb title on page
    Mouse Over    xpath=//a[@title='Kitchen Equipment']
    Sleep    5s
    Mouse Over    xpath=//a[@title='Cooking Equipment']
    Sleep    5s
    Click Element    xpath=(//a[@title='Brazing Pans'])[2]
    Element Should Be Visible    xpath=//div[@id='breadcrumb']    #verifies breadcrumbs visible on page
    Log To Console    Breadcrumbs are visible
    Click Element    xpath=//ol[@class='breadcrumb']//a[contains(text(),'Cooking Equipment')]    #Navigates to cooking equipment page
    Sleep    5s
    Click Element    xpath=//ol[@class='breadcrumb']//a[contains(text(),'Kitchen Equipment')]    #Navigates to kitchen equipment page
    Sleep    5s
    Click Element    xpath=//a[contains(text(),'Home')]    #Navigates to home page

MyAccount
    [Setup]    openbrowser    https://accstorefront.c4f4c56-foodservi1-s1-public.model-t.cc.commerce.ondemand.com/fsxstorefront/en_US/    chrome
    Maximize Browser Window
    Click Element    id=signin-popup
    Sleep    3s
    Click Element    xpath=//form[@id='loginajaxform']//input[@name='j_username']
    Input Text    xpath=//form[@id='loginajaxform']//input[@name='j_username']    @{EnterUsername}[0]
    Sleep    3s
    Click Element    id=j_password
    Input Text    id=j_password    12341234
    Sleep    3s
    Click Element    id=loginsubmit
    Sleep    5s
    click element    xpath=//a[@id='myaccount-popup']    #click on my account section
    sleep    2s
    click element    xpath=//a[text()='Account Management ']    #Click on account management
    sleep    2s
    Personal Information
    sleep    2s
    ${Phone Number}    Get Text    xpath=//input[@id='phoneNumber']    #4.To check user is able to see the customer number under Personal information section
    sleep    2s
    ${First Name}    Get Text    xpath=//input[@id='accFirstName']
    click element    xpath=//button[@class='btn btn-primary acc-btn']    #click on save button
    ${GetText}    Get Text    xpath=//div[@class='global-alerts']
    click element    xpath=//a[contains(text(),'Cancel')]    #click on cancel button
    sleep    2s
    Change Email Address
    Change Password
    Address
    #Order detail page

Cart
    [Setup]    openbrowser    https://accstorefront.c4f4c56-foodservi1-s1-public.model-t.cc.commerce.ondemand.com/fsxstorefront/en_US/    chrome
    Maximize Browser Window
    Verify cart icon
    Sleep    2s
    Click Element    xpath=//div[contains(@class,'yCmsComponent col-xs-5 col-sm-4 col-md-3 navbar-header__logo')]//img[@class='img-responsive']
    Sleep    2s
    Click Element    xpath=//div[@class='user-cart-navbar']
    Sleep    2s
    Element Should Contain    xpath=//a[@class='btn btn-link btn-go2cart']    Go To Shopping Cart
    Sleep    2s
    ${items}    Get Text    xpath=//div[@class='mini-cart__itemtotal']
    #Element Text Should Be    xpath=//div[@class='mini-cart__notitems']    Your shopping cart is currently empty
    Sleep    2s
    #Go To    https://accstorefront.c4f4c56-foodservi1-s1-public.model-t.cc.commerce.ondemand.com/en_US/FSX/Kitchen-Equipment/Bakery/Dough-Sheeter/Manual-Dough-Divider-30-Parts/p/mp-00000029
    Go To    https://accstorefront.c4f4c56-foodservi1-s1-public.model-t.cc.commerce.ondemand.com/en_US/FSX/Light-Counter-Equipment/Food-Warmers/Countertop/Vollrath-71001-Cayenne-Full-Size-Countertop-Warmer-with-Stainless-Steel-Exterior-120V-700W/p/mp-00002519
    Sleep    2s
    Click Element    id=addToCartButton
    Sleep    2s
    ${items}    Get Text    xpath=//div[@class='mini-cart__itemtotal']
    Sleep    2s
    Element Should Be Visible    xpath=//img[contains(@class,'img-responsive mCS_img_loaded')]    #Product image is visible in minicart.
    Sleep    2s
    Element Should Be Visible    xpath=//h2[contains(@class,'mini-cart__item_title')]    #Verifies product title in minicart
    ${sku}    Get Text    xpath=//div[contains(text(),'SKU:')]
    Sleep    2s
    ${qty}    Get Text    xpath=//div[contains(text(),'QTY:')]
    Sleep    2s
    ${subtotal}    Get Text    xpath=//div[@class='mini-cart__ordersubtotal']
    ${itemprice}    Get Text    xpath=//div[@class='mini-cart__item_price']
    Sleep    2s
    Click Element    xpath=//div[@class='user-cart-navbar']
    Sleep    2s
    Element Should Be Visible    xpath=//a[@class='btn btn-primary btn-viewcart']    #verifies proceed to checkout button in minicart icon
    Sleep    2s
    Click Element    xpath=//a[contains(text(),'Kitchen Equipment')]
    Sleep    2s
    Add 3 produucts to cart
    Sleep    5s
    Scroll Element Into View    xpath=//img[contains(@class,'img-responsive mCS_img_loaded')]    #Product image is visible in minicart.
    Sleep    2s
    Click Element    xpath=//img[contains(@class,'img-responsive mCS_img_loaded')]    #Clicks on Product image
    Sleep    2s
    Click Element    xpath=//div[@class='user-cart-navbar']    #clicks on cart icon
    Scroll Element Into View    xpath=//h2[contains(@class,'mini-cart__item_title')]    #Verifies product title in minicart
    Click Element    xpath=//h2[contains(@class,'mini-cart__item_title')]
    Sleep    2s
    Click Element    xpath=//div[@class='user-cart-navbar']    #clicks on cart icon
    Cart page

PDP
    [Setup]    openbrowser    https://accstorefront.c4f4c56-foodservi1-s1-public.model-t.cc.commerce.ondemand.com/fsxstorefront/en_US/    chrome
    Maximize Browser WIndow
    Click Element    id=signin-popup
    Click Element    xpath=//form[@id='loginajaxform']//input[@name='j_username']
    Input Text    xpath=//form[@id='loginajaxform']//input[@name='j_username']    @{EnterUsername}[3]
    Click Element    id=j_password
    Input Text    id=j_password    12341234
    Sleep    3s
    Click Element    id=loginsubmit
    sleep    5s
    Mouse Over    xpath=//a[@title='Furniture']    #over mouse on furniture category
    sleep    5s
    mouse over    xpath=//a[@title='Kitchen Equipment']    #over mouse on Kitchen Equipment
    sleep    5s
    mouse over    xpath=//a[@title='Cooking Equipment']    #Over mouse on cooking equipment
    sleep    5s
    click element    xpath=(//a[contains(text(),'Ranges')])[2]    #select ranges category
    sleep    5s
    #go to    https://accstorefront.c4f4c56-foodservi1-s1-public.model-t.cc.commerce.ondemand.com/en_US/FSX/Kitchen-Equipment/Cooking-Equipment/Ranges/Standard/Sterno-Products-50162-Butane-Fuel-Refill-8-oz-Canister-12-Case/p/mp-00000069    # for free shipping product
    go to    https://accstorefront.c4f4c56-foodservi1-s1-public.model-t.cc.commerce.ondemand.com/en_US/FSX/Kitchen-Equipment/Cooking-Equipment/Ranges/Standard/Garland-ED-15H-Designer-Series-15-Two-Open-Burner-Electric-Countertop-Hot-Plate-240V-1-Phase-4-2-kW/p/mp-00000107    #select the product contains name \ :-Garland SP-1844-2 Liquid Propane Double Countertop Stock Pot Stove with 6" Legs - 90,000 BTU
    Page Should Contain Image    id=xzoom-default    #To verify product image on PDP
    mouse over    id=xzoom-default    #To verify image can be magnified
    Page Should Contain Element    xpath=//h1[contains(text(),SP-1844-2)]    Garland SP-1844-2 Liquid Propane Double Countertop Stock Pot Stove with 6" Legs - 90,000 BTU    #to verify PDP contains product name
    Element Should Be Visible    xpath=//p[contains(@class,'similar-products__data_number')]    Product SKU: mp-00000107    #To verify prodct has prodcut sku
    Element Should Contain    xpath=//div[@class='similar-products__data_stock']/p[2]    In Stock    #To verify status of product
    ${Sellerdetail}    Get Text    id=seller-details    #To verify seller name
    ${MarketFsxPrice}    Get Text    id=productPriceDisplay    # To verify market price and FSX price is displayed on page.
    Element Attribute Value Should Be    id=qty    placeholder    1    #To verify user is able to see the quantity field which is prefilled with 1 qty in PDP
    Element Should Be Visible    id=addToCartButton    #To verify user is able to see Add to cart button in PDP
    sleep    2s
    Mouse Over    id=addToCartButton    # To verify add to cart button is visible
    Click Button    id=addToCartButton    # To verify user can click add to cart button
    sleep    2s
    click element    xpath=//a[text()='Add To Wishlist']    # To verify user can click add to wishlist
    ${GetColor}    Get Text    xpath=//p[@class='text-red']    #Get error text
    sleep    5s
    #element should be visible    xpath=//form//div//div[1]//label[1]    #wishlist text should be visible inside wishlist
    #Selenium2Library.Input Text    xpath=//input[@id='input-nameyourlist']    my wishlist1    #Enter wishlist name in filed
    Comment    click element    xpath=//button[@id='createListSubmit']    #click create wishlist
    comment    10s
    #element should be visible    xpath=//div[@class='text-success add-to-list_success']    #To verify user is able to view success message by clicking on create wishlist
    sleep    5s
    click element    xpath=//div[@id='addToListModal']//button[@class='close']    # to verify use is able to close wishlist popup after successful creation of wishlist
    sleep    2s
    Element Should Contain    xpath=(//h4[@class='panel-title'])[1]    Product Specification    #verify user can view product description
    sleep    2s
    click element    xpath=(//div[@class='panel-heading'])[1]
    sleep    2s
    Element Should Be Visible    xpath=//div[@class='estimate-shipping']
    sleep    2s
    Element Should Be Disabled    id=sub-estimated-shipping-btn    #when no zip code is entered in filed then submit button should be disabled
    Input Text    id=miraklEstimatedShippingCost    @{ZipCode}[0]    #enter zip to estimate cost
    click element    id=sub-estimated-shipping-btn    #click submit
    sleep    5s
    Element Should Be Visible    xpath=(//span[@class='text-blue'])[3]    #To verify shipping cost message by clicking on submit button of estimate cost
    Input Text    id=miraklEstimatedShippingCost    @{ZipCode}[1]
    sleep    2s
    click element    id=sub-estimated-shipping-btn
    Element Text Should Be    xpath=//div/label[text()='Please enter valid zip code']    Please enter valid zip code
    Input Text    id=miraklEstimatedShippingCost    @{ZipCode}[2]
    sleep    2s
    click element    id=sub-estimated-shipping-btn
    sleep    5s
    #Selenium2Library.Input Text    id=miraklEstimatedShippingCost    Selenium2Library.Get Text
    click element    xpath=//h4[contains(text(),'Product Specification')]

Compare Product
    [Setup]    openbrowser    https://accstorefront.c4f4c56-foodservi1-s1-public.model-t.cc.commerce.ondemand.com/fsxstorefront/fsx/en_US/    chrome
    Maximize Browser Window
    sleep    5s
    ${Title}    Get title
    Title Should Be    ${Title}
    click element    xpath=(//span[@class='yCmsComponent wrapper__link'])[1]    #Click on kitchen equipment
    sleep    5s
    Click Element    xpath=(//div[@class='checkbox checkbox-inline checkbox-compare'])[3]    #click on compare check box of microwave4 product
    sleep    5s
    Click element    xpath=(//div[@class='checkbox checkbox-inline checkbox-compare'])[2]    #select compare check box first sap dev product.
    sleep    2s
    Click element    xpath=(//div[@class='checkbox checkbox-inline checkbox-compare'])[4]    #select comare check box of second sap dev product
    sleep    2s
    Click element    xpath=(//div[@class='checkbox checkbox-inline checkbox-compare'])[1]    #selct compare check box of text fsx product
    sleep    5s
    click element    xpath=(//div[@class='checkbox checkbox-inline checkbox-compare'])[8]    #select compare box of product Beverage-Air SM58N-W 58 1/2" White 1-Sided Cold Wall Milk Cooler
    sleep    2s
    ${GetCompare error}    Get Text    xpath=/html/body/main/div[1]/div[2]/div/div/div/div[3]/div[2]/div/div[1]/div[3]    #verifies if more then 4 product is selected get error text
    Scroll Element Into View    xpath=/html/body/main/div[1]/div[2]/div/div/div/div[3]/div[2]/div/div[1]/div[2]/div[2]/form/button
    click button    xpath=/html/body/main/div[1]/div[2]/div/div/div/div[3]/div[2]/div/div[1]/div[2]/div[2]/form/button    #click on comapre button
    sleep    5s
    Element text should be    xpath=//*[@id="breadcrumb"]/ol    Home Compare Items    #To verify whether user is able to see the breadcrumb as 'Home > Compare Items' on compare pagae
    click element    xpath=/html/body/main/div[1]/div[2]/div/section/div[1]/div[1]/a    #verify back button is visible and clickable
    sleep    5s
    Go back
    Click element    xpath=//*[@id="breadcrumb"]/ol/li[1]/a    #Verify home link is enable mode
    sleep    5s
    go back
    Element Text Should Be    xpath=//h1/strong[text()='Compare Items']    Compare Items    #verifies compare items text on compare page
    ${Gettext}    Get Text    xpath=//div/span[@class='compare-count']
    #Element Should Contain    xpath=(//div[@class='product-column-javax.servlet.jsp.jstl.core.LoopTagSupport$1Status@3e6244d product-item product-title'])[1]    Second SAP Dev Product \ Product #:mp-00000021 \ OUT OF STOCK
    click element    xpath=/html/body/main/div[1]/div[2]/div/section/div[2]/table/thead/tr/th[3]/a    #click on remove button (cross symbol) of product
    element should be visible    xpath=(//a/img[@class='img-responsive'])[2]    #Verify image is available
    Click Image    xpath=(//a/img[@class='img-responsive'])[2]    #click image and navigate to PDP
    Go back    #Go back to compare page
    ${Height}    ${Width}    Get Element Size    xpath=(//a/img[@class='img-responsive'])[2]
    Element Should Be Visible    xpath=//div/ul[@class='main-navigation__navbar']    #Verify bredcrumb is visible in comapre page
    Click Element    xpath=(//a[text()='Add To Wishlist'])[1]    #click on wishlist button
    sleep    5s
    #click element    xpath=(//div[@class='modal-dialog'])[6]    # to click on pop up
    #click element    xpath=(//div[@class='modal-content'])[6]    # to click on popup
    Click Element    xpath=//*[@id="addToListUnauthenticatedUser"]/div/div/div[1]/button    #close the pop up of wishlist
    Element Should Be Visible    id=question    #Search icon is visible
    Element Should Be Enabled    id=addToCartButton    #Add to cart should enable if stock status is enabled
    Element Should Be Disabled    xpath=//button[@disabled='disabled']    #Add to cart button is disabled for Out of stock products
    Scroll Element Into View    id=addToCartButton
    click element    id=addToCartButton
    Element Should Be Enabled    xpath=//div[@class='user-cart-navbar']    #verify cart icon is visbible and its enabled

checkout
    [Setup]    openbrowser    https://accstorefront.c4f4c56-foodservi1-s1-public.model-t.cc.commerce.ondemand.com/fsxstorefront/fsx/en_US/    chrome
    Maximize Browser Window
    Sleep    2s
    Click Element    id=signin-popup    \    #Login pop up window is displayed
    Click Element    xpath=//form[@id='loginajaxform']//input[@name='j_username']    \    #clicks on username field
    Input Text    xpath=//form[@id='loginajaxform']//input[@name='j_username']    @{EnterUsername}[2]
    Click Element    id=j_password
    Input Text    id=j_password    12341234
    Sleep    3s
    Click Element    id=loginsubmit    \    #clicks login button
    Sleep    2s
    #Go To    https://fsx-qa.pragiti.com/fsxstorefront/fsx/en_US//Garland/Garland-SP-1844-2-Liquid-Propane-Double-Countertop-Stock-Pot-Stove-with-6-Legs-90-000-BTU/p/fsx-00003267
    Go To    https://accstorefront.c4f4c56-foodservi1-s1-public.model-t.cc.commerce.ondemand.com/en_US/FSX/Light-Counter-Equipment/Food-Warmers/Countertop/Vollrath-71001-Cayenne-Full-Size-Countertop-Warmer-with-Stainless-Steel-Exterior-120V-700W/p/mp-00002519    #clicks on header Kitchen Equipment    #Root Header
    Sleep    2s
    #Click Element    xpath=//div[17]//div[1]//div[1]//div[1]//a[1]//img[1]    #navigate to pdp
    Sleep    5s
    Click Element    xpath=//button[@id='addToCartButton']    #add product to cart
    Sleep    2s
    Reload Page
    Click Element    xpath=//div[@class='user-cart-navbar']
    Sleep    25s
    Click Element    xpath=//a[@class='btn btn-primary btn-viewcart']
    Sleep    2s
    BuiltIn.Run Keyword If    '${status}' == 'PASS'    Click Element    xpath=//div[contains(@class,'checkout-steps')]//div[1]//div[1]//div[1]//a[1]    ELSE...    Click Element    xpath=//*[@id="addressCode"]/div[2]/input
    Sleep    2s
    #Click Element    xpath=//*[@id="addressCode"]/div[2]/input
    Sleep    2s
    Click Element    xpath=//*[@id="deliveryMethodNext"]
    Sleep    5s
    #Click Element    xpath=//input[@class='btn btn-primary js-submitform']
    Sleep    2s
    Click Element    xpath=//label[contains(text(),'Use my Shipping Address')]
    Sleep    2s
    Click Element    xpath=//input[@class='btn btn-primary js-submitform tax-cert-check']
    Sleep    2s
    Click Element    xpath=//input[@id='Terms1']
    Sleep    2s
    Click Element    xpath=//button[@id='cardSubmitAuthorize']
    Sleep    2s
    Sleep    10s
    Select Frame    xpath=//div[@id='AcceptUIContainer']//iframe
    Sleep    10s
    Input Text    xpath=//input[@class='testCardNumberInput ng-untouched ng-pristine ng-valid']    @{card}
    Sleep    2s
    Input Text    xpath=//input[@id='expiryDate']    0221
    Sleep    2s
    Input Text    xpath=//input[@id='cvv']    223
    Sleep    2s
    Click Element    xpath=//*[@id="payButton"]

Forgot Password
    [Setup]    openbrowser    https://accstorefront.c4f4c56-foodservi1-s1-public.model-t.cc.commerce.ondemand.com/fsxstorefront/en_US/    chrome
    Maximize Browser Window
    sleep    5s
    click element    id=signin-popup    #click on login button
    sleep    2s
    click link    xpath=(//p/a[@class='forgot-password-link'])[1]
    sleep    5s
    click element    xpath=//*[@id="forgotPasswordModal"]/div/div
    click element    xpath=//input[@class='form-control js-input-customerror']
    Input Text    xpath=//input[@class='form-control js-input-customerror']    @{Email}[0]    #enter email which is not registred
    click element    xpath=//button[@class='btn btn-primary forgotPasswordBtn']    #click resent pwd link
    sleep    5s
    Element Text Should Be    xpath=//label[@class='error error-custom']    Username or Email Address does not exist.    #verifies the error message
    sleep    5s
    Clear Element Text    xpath=//input[@class='form-control js-input-customerror']    # clears the text present inside forgot pwd field
    Input Text    xpath=//input[@class='form-control js-input-customerror']    @{Email}[1]    #enter registered email id
    click element    xpath=//button[@class='btn btn-primary forgotPasswordBtn']    #click resent pwd link    # click on submit button
    Sleep    5s
    Element Should Contain    xpath=//div[@id='resposeMsg']    Password reset instructions have been sent to your username or e-mail address : soumya@yopmail.com
    sleep    15s
    click element    xpath=//*[@id="forgotPasswordModal"]/button
    sleep    5s
    click element    id=myheaderwishlist
    element should be visible    xpath=(//a[@class='forgot-password-link'])[1]    #verify forgot pwd link is vivible on whishlist pop-up
    Go To    http://www.yopmail.com/
    sleep    5s
    Input Text    xpath=//input[@id='login']    soumya@yopmail.com
    sleep    2s
    click element    xpath=//input[@class='sbut']
    sleep    15s
    Reload Page
    #Wait Until Element Is Enabled    xpath=(//span[text()='Customer Service'])[1]    20s
    #click element    xpath=(//span[text()='Customer Service'])[1]    #click on the mail
    sleep    15s
    Select Frame    xpath=//*[@id="ifmail"]
    Sleep    2s
    click link    xpath=//a[contains(text(),'Reset')]    #click on reset my pwd link
    sleep    5s
    Select Window    NEW
    Click Element    id=password
    Input Text    id=password    12341234
    sleep    2s
    Input Text    id=confirmPwd    12341234
    sleep    2s
    click element    xpath=//button[contains(text(),'Reset Password')]
    sleep    2s

*** Keywords ***
countryphoneno
    Click Element    xpath=//span[contains(text(),'United States')]

Kitchen PLP Compare
    sleep    5s
    ${Title}    Get title
    Title Should Be    ${Title}
    click element    xpath=(//span[@class='yCmsComponent wrapper__link'])[1]    #Click on kitchen equipment
    sleep    5s
    Click Element    xpath=(//div[@class='checkbox checkbox-inline checkbox-compare'])[3]    #click on compare check box of microwave4 product
    sleep    5s
    Click element    xpath=(//div[@class='checkbox checkbox-inline checkbox-compare'])[2]    #select compare check box first sap dev product.
    sleep    5s
    Click element    xpath=(//div[@class='checkbox checkbox-inline checkbox-compare'])[4]    #select comare check box of second sap dev product
    sleep    5s    \    #verifies product title is visible
    Click element    xpath=(//div[@class='checkbox checkbox-inline checkbox-compare'])[1]    #selct compare check box of text fsx product
    sleep    5s
    click element    xpath=(//div[@class='checkbox checkbox-inline checkbox-compare'])[8]    #select compare box of product Beverage-Air SM58N-W 58 1/2" White 1-Sided Cold Wall Milk Cooler
    sleep    2s
    ${GetCompare error}    Get Text    xpath=/html/body/main/div[1]/div[2]/div/div/div/div[3]/div[2]/div/div[1]/div[3]    #verifies if more then 4 product is selected get error text
    Scroll Element Into View    xpath=/html/body/main/div[1]/div[2]/div/div/div/div[3]/div[2]/div/div[1]/div[2]/div[2]/form/button
    Click Element    xpath=/html/body/main/div[1]/div[2]/div/div/div/div[3]/div[2]/div/div[1]/div[2]/div[2]/form/button    #click on comapre button
    sleep    5s

Change Email Address
    sleep    2s
    Element Should Be Visible    xpath=//ul[@class='main-navigation__navbar']    #To verify bredcrumbs are availabe in my account section
    sleep    3s
    Click Element    xpath=//a[contains(text(),'Change Email Address')]    #user is able to click on change email address link
    Sleep    3s
    Element Text Should Be    xpath=//h1[contains(@class,'acc-main__header_title')]    Email Address
    Sleep    3s
    Element Text Should Be    xpath=//label[contains(text(),'Confirm New Email Address')]    Confirm New Email Address
    Sleep    3s
    Click Element    xpath=//input[@id='confirmnew']
    Sleep    3s
    Input Text    //input[@id='confirmnew']    riteshgmail.com
    Sleep    3s
    Click Element    xpath=//button[@class='btn btn-primary acc-btn']    #click on update button.
    Element Text Should Be    xpath=//label[contains(text(),'Please enter a valid email.')]    Please enter a valid email.
    Sleep    3s
    Click Element    id=communicationemailaddress
    Sleep    2s
    Clear Element Text    id=communicationemailaddress    #removes entered email address
    Sleep    5s
    Element Text Should Be    xpath=//button[@class='btn btn-primary acc-btn']    Update    #Update button is visible or not
    Sleep    2s
    Click Element    xpath=//button[@class='btn btn-primary acc-btn']    #click on update button
    Sleep    5s
    Element Text Should Be    xpath=//label[@class='form-error']    This is a required field.
    Sleep    3s
    Click Element    xpath=//input[@id='communicationemailaddress']
    Sleep    3s
    Input Text    xpath=//input[@id='communicationemailaddress']    @{EnterUsername}[0]
    Click Element    xpath=//input[@id='confirmnew']
    Sleep    3s
    Input Text    xpath=//input[@id='confirmnew']    @{EnterUsername}[0]
    Sleep    3s
    Click Element    xpath=//button[@class='btn btn-primary acc-btn']    #clicks on update button
    Sleep    3s
    Element Should Contain    xpath=/html[1]/body[1]/main[1]/div[1]/div[2]/div[1]    You have entered your existing email address. To change, please enter a new email address
    Sleep    2s
    Element Text Should Be    xpath=//a[contains(text(),'Cancel')]    Cancel
    Sleep    3s
    Click Element    xpath=//a[@class='btn-link']
    Sleep    3s
    Click Element    xpath=//a[contains(text(),'Change Email Address')]    #user is able to click on change email address link
    Sleep    3s
    Click Element    xpath=//input[@id='communicationemailaddress']
    Clear Element Text    xpath=//input[@id='communicationemailaddress']    #removes entered email address
    Input Text    xpath=//input[@id='communicationemailaddress']    @{EnterUsername}[1]
    Click Element    xpath=//input[@id='confirmnew']
    Sleep    2s
    Clear Element Text    id=confirmnew    #removes entered email address
    Sleep    3s
    Input Text    //input[@id='confirmnew']    @{EnterUsername}[1]
    Click Element    xpath=//button[@class='btn btn-primary acc-btn']    #click on update button
    Sleep    2s
    Element Should Contain    xpath=//div[@class='alert alert-info alert-dismissable getAccAlert']    Email address is updated    #verfies success msg
    Sleep    2s

Address
    ShipAddress
    Sleep    2s
    Bill Address

Ship Address details
    Click Element    id=billinginputSchoolName
    Input Text    id=billinginputSchoolName    @{Address}[0]
    Sleep    2s
    Click Element    id=billinginputFirstName
    Input Text    id=billinginputFirstName    @{Address}[1]
    Sleep    2s
    Click Element    id=billinginputLastName
    Input Text    id=billinginputLastName    @{Address}[2]
    Sleep    2s
    Click Element    id=billinginputAddress1
    Input Text    id=billinginputAddress1    @{Address}[3]
    Sleep    2s
    Click Element    id=billinginputCityName
    Input Text    id=billinginputCityName    @{Address}[4]
    Sleep    2s
    Click Element    id=js-region
    Select From List By Value    id=js-region    @{Address}[5]
    Sleep    2s
    Click Element    id=billinginputZIPCode
    Input Text    id=billinginputZIPCode    @{Address}[6]
    Sleep    2s
    Click Element    id=billinginputPhoneNumber
    Input Text    id=billinginputPhoneNumber    @{Address}[7]
    Sleep    2s

ShipAddress
    Click Element    xpath=//a[contains(text(),'Addresses')]
    Sleep    2s
    Element Text Should Be    xpath=//h1[@class='acc-main__header_title']    Address Book
    Sleep    2s
    Element Text Should Be    xpath=//h3[contains(text(),'Shipping Addresses')]    Shipping Addresses
    Sleep    2s
    Element Text Should Be    xpath=//strong[contains(text(),'You have no Shipping Address')]    You have no Shipping Address
    Sleep    2s
    Element Text Should Be    xpath=//a[contains(text(),'Add New Shipping Address')]    Add New Shipping Address
    Sleep    2s
    Click Element    xpath=//a[contains(text(),'Add New Shipping Address')]
    Sleep    2s
    Click Element    xpath=//input[contains(@class,'btn btn-primary')]    #clicks on submit button
    Sleep    2s
    Element Text Should Be    id=billinginputSchoolName-error    This field is required.
    Element Text Should Be    id=billinginputFirstName-error    This field is required.
    Element Text Should Be    id=billinginputLastName-error    This field is required.
    Element Text Should Be    id=billinginputAddress1-error    This field is required.
    Element Text Should Be    id=billinginputCityName-error    This field is required.
    Element Text Should Be    id=js-region-error    This field is required.
    Element Text Should Be    id=billinginputZIPCode-error    Please enter zip code
    Element Text Should Be    id=billinginputPhoneNumber-error    This field is required.
    Sleep    2s
    Element Text Should Be    xpath=//label[contains(text(),'Country')]    Country
    Element Text Should Be    xpath=//label[contains(text(),'Company Name')]    Company Name
    Element Text Should Be    xpath=//label[contains(@class,'control-label')][contains(text(),'First Name')]    First Name
    Element Text Should Be    xpath=//label[contains(text(),'Last Name')]    Last Name
    Element Text Should Be    xpath=//label[contains(text(),'Address Line 1')]    Address Line 1
    Element Text Should Be    xpath=//label[contains(text(),'Address Line 2')]    Address Line 2
    Element Text Should Be    xpath=//label[contains(text(),'City')]    City
    Element Text Should Be    xpath=//label[contains(text(),'State')]    State
    Element Text Should Be    xpath=//label[contains(text(),'ZIP Code')]    ZIP Code
    Element Text Should Be    xpath=//label[contains(text(),'Phone Number')]    Phone Number
    Sleep    2s
    Click Element    id=billinginputFirstName
    Input Text    id=billinginputFirstName    123abc
    Element Text Should Be    id=billinginputFirstName-error    Please enter a valid First Name
    Sleep    2s
    Click Element    id=billinginputLastName
    Input Text    id=billinginputLastName    12$%^^&*
    Element Text Should Be    id=billinginputLastName-error    Please enter a valid Last Name
    Sleep    2s
    Click Element    id=billinginputZIPCode
    Input Text    id=billinginputZIPCode    4321
    Element Text Should Be    id=billinginputZIPCode-error    Please enter valid zip code
    Sleep    2s
    Click Element    id=billinginputPhoneNumber
    Input Text    id=billinginputPhoneNumber    qwer
    Element Text Should Be    id=billinginputPhoneNumber-error    Please enter a valid phone number.
    Sleep    2s
    Click Element    xpath=//a[@class='btn-link']    #verifies cancel button
    Sleep    10s
    Click Element    xpath=//a[contains(text(),'Add New Shipping Address')]
    Sleep    2s
    Radio Button Should Be Set To    myaccountaddress    shipTo    #verifies shipping address radio button is preselected
    Sleep    2s
    Ship Address details
    Click Element    xpath=//input[@class='btn btn-primary']    #clicks on submit button
    Sleep    5s
    #Click Element    xpath=/html/body/main/div[1]/div[3]/div[2]/div[2]/div/div[2]/div/div[1]/div[1]/div/div/p[3]/a    #click on default link
    Sleep    5s
    #Element Should Contain    xpath=//div[@class='acc-main__block_addr default row-eq-height']    Default Shipping Address    #verifies address available below ship address.
    #Element Text Should Be    xpath=//p[contains(text(),'Default Shipping Address')]    Default Shipping Address
    #Element Text Should Be    xpath=//p[@class='acc-main__block_addr_default-msg']    To delete, select another Shipping address as your default
    Sleep    5s
    Click Element    xpath=//a[text()=' Add New Shipping Address']    #clicks on Add new address link
    Ship Address detail2
    Click Element    xpath=//input[@class='btn btn-primary']    #clicks on submit button
    Sleep    2s
    Element Should Contain    xpath=//div[@class='acc-main__block_addr row-eq-height']    Edit
    Element Should Contain    xpath=//div[@class='acc-main__block_addr row-eq-height']    Delete
    Element Should Contain    xpath=//div[@class='acc-main__block_addr row-eq-height']    Make Default
    Sleep    5s
    #Click Element    xpath=//div[@class='acc-main__block_addr row-eq-height']
    Click Element    xpath=//a[contains(text(),'Make Default')]    #makes second address as default address
    Sleep    2s
    Click Element    //div[@class='acc-main__block_addr row-eq-height']//a[contains(text(),'Delete')]    #clicks on delete link
    Sleep    2s
    Click Element    xpath=(//button[@class='close'])[2]    #click on delete adress pop up close icon
    Sleep    2s
    Click Element    //div[@class='acc-main__block_addr row-eq-height']//a[contains(text(),'Delete')]    #clicks on delete link
    Sleep    2s
    Click Element    xpath=(//a[@class='btn btn-link'][contains(text(),'Cancel')])[2]    #clicks on cancel link
    Sleep    2s
    Click Element    //div[@class='acc-main__block_addr row-eq-height']//a[contains(text(),'Delete')]    #click on Delete link
    Sleep    2s
    Element Text Should Be    xpath=(//h4[@class='modal-title'])[2]    Delete Address
    Sleep    2s
    Click Element    xpath=(//button[@class='btn btn-primary modal-btn'][contains(text(),'Delete')])[2]    #delete 2nd address
    Sleep    2s
    Click Element    xpath=//a[contains(text(),'Edit')]
    Sleep    4s
    edit address
    Click Element    xpath=//input[@class='btn btn-primary']    #clicks on submit button
    Sleep    5s
    Click Element    xpath=//a[contains(text(),'Delete')]    #clicks 1st address delete link
    Sleep    2s
    Click Element    xpath=//button[@class='btn btn-primary modal-btn']    #clicks on delete button from the pop up
    Sleep    2s
    Element Should Contain    class=global-alerts    Your address was removed.

Ship Address detail2
    Click Element    id=billinginputSchoolName
    Input Text    id=billinginputSchoolName    @{Address}[8]
    Sleep    2s
    Click Element    id=billinginputFirstName
    Input Text    id=billinginputFirstName    @{Address}[9]
    Sleep    2s
    Click Element    id=billinginputLastName
    Input Text    id=billinginputLastName    @{Address}[10]
    Sleep    2s
    Click Element    id=billinginputAddress1
    Input Text    id=billinginputAddress1    @{Address}[11]
    Sleep    2s
    Click Element    id=billinginputCityName
    Input Text    id=billinginputCityName    @{Address}[12]
    Sleep    2s
    Click Element    id=js-region
    Select From List By Value    id=js-region    @{Address}[13]
    Sleep    2s
    Click Element    id=billinginputZIPCode
    Input Text    id=billinginputZIPCode    @{Address}[14]
    Sleep    2s
    Click Element    id=billinginputPhoneNumber
    Input Text    id=billinginputPhoneNumber    @{Address}[15]
    Sleep    2s
    Element Text Should Be    xpath=//label[contains(text(),'Make Default')]    Make Default

edit address
    Click Element    id=billinginputSchoolName
    Input Text    id=billinginputSchoolName    @{Address}[16]
    Sleep    2s
    Click Element    id=billinginputFirstName
    Input Text    id=billinginputFirstName    @{Address}[17]
    Sleep    2s
    Click Element    id=billinginputLastName
    Input Text    id=billinginputLastName    @{Address}[18]
    Sleep    2s
    Click Element    id=billinginputAddress1
    Input Text    id=billinginputAddress1    @{Address}[19]
    Sleep    2s
    Click Element    id=billinginputCityName
    Input Text    id=billinginputCityName    @{Address}[20]
    Sleep    2s
    Click Element    id=js-region
    Select From List By Value    id=js-region    @{Address}[21]
    Sleep    2s
    Click Element    id=billinginputZIPCode
    Input Text    id=billinginputZIPCode    @{Address}[22]
    Sleep    2s
    Click Element    id=billinginputPhoneNumber
    Input Text    id=billinginputPhoneNumber    @{Address}[23]
    Sleep    2s

Bill Address
    Element Text Should Be    xpath=//h3[contains(text(),'Billing Addresses')]    Billing Addresses
    Sleep    2s
    Element Text Should Be    xpath=//strong[contains(text(),'You have no Billing Address')]    You have no Billing Address
    Sleep    2s
    Element Text Should Be    xpath=//a[contains(text(),'Add New Billing Address')]    Add New Billing Address
    Sleep    8s
    Click Element    xpath=//a[contains(text(),'Add New Billing Address')]
    Sleep    2s
    Click Element    xpath=//input[contains(@class,'btn btn-primary')]    #clicks on submit button
    Sleep    2s
    Element Text Should Be    id=billinginputSchoolName-error    This field is required.
    Element Text Should Be    id=billinginputFirstName-error    This field is required.
    Element Text Should Be    id=billinginputLastName-error    This field is required.
    Element Text Should Be    id=billinginputAddress1-error    This field is required.
    Element Text Should Be    id=billinginputCityName-error    This field is required.
    Element Text Should Be    id=js-region-error    This field is required.
    Element Text Should Be    id=billinginputZIPCode-error    Please enter zip code
    Element Text Should Be    id=billinginputPhoneNumber-error    This field is required.
    Sleep    2s
    Element Text Should Be    xpath=//label[contains(text(),'Country')]    Country
    Element Text Should Be    xpath=//label[contains(text(),'Company Name')]    Company Name
    Element Text Should Be    xpath=//label[contains(@class,'control-label')][contains(text(),'First Name')]    First Name
    Element Text Should Be    xpath=//label[contains(text(),'Last Name')]    Last Name
    Element Text Should Be    xpath=//label[contains(text(),'Address Line 1')]    Address Line 1
    Element Text Should Be    xpath=//label[contains(text(),'Address Line 2')]    Address Line 2
    Element Text Should Be    xpath=//label[contains(text(),'City')]    City
    Element Text Should Be    xpath=//label[contains(text(),'State')]    State
    Element Text Should Be    xpath=//label[contains(text(),'ZIP Code')]    ZIP Code
    Element Text Should Be    xpath=//label[contains(text(),'Phone Number')]    Phone Number
    Sleep    2s
    Click Element    id=billinginputFirstName
    Input Text    id=billinginputFirstName    123abc
    Element Text Should Be    id=billinginputFirstName-error    Please enter a valid First Name
    Sleep    2s
    Click Element    id=billinginputLastName
    Input Text    id=billinginputLastName    12$%^^&*
    Element Text Should Be    id=billinginputLastName-error    Please enter a valid Last Name
    Sleep    2s
    Click Element    id=billinginputZIPCode
    Input Text    id=billinginputZIPCode    4321
    Element Text Should Be    id=billinginputZIPCode-error    Please enter valid zip code
    Sleep    2s
    Click Element    id=billinginputPhoneNumber
    Input Text    id=billinginputPhoneNumber    qwer
    Element Text Should Be    id=billinginputPhoneNumber-error    Please enter a valid phone number.
    Sleep    2s
    Click Element    xpath=//a[@class='btn-link']    #verifies cancel button
    Sleep    10s
    Click Element    xpath=//a[contains(text(),'Add New Billing Address')]
    Sleep    2s
    Radio Button Should Be Set To    myaccountaddress    billTo
    Sleep    2s
    Bill Address details
    Click Element    xpath=//input[@class='btn btn-primary']    #clicks on submit button
    Sleep    5s
    Element Should Contain    xpath=//div[@class='acc-main__block_addr default row-eq-height']    Default Billing Address
    Element Text Should Be    xpath=//p[contains(text(),'Default Billing Address')]    Default Billing Address
    Element Text Should Be    xpath=//p[@class='acc-main__block_addr_default-msg']    To delete, select another Billing address as your default
    Sleep    5s
    Click Element    xpath=//a[contains(text(),'Add New Billing Address')]    #clicks on Add new address link
    Bill Adress detail2
    Click Element    xpath=//input[@class='btn btn-primary']    #clicks on submit button
    Sleep    2s
    Element Should Contain    xpath=//div[@class='acc-main__block_addr row-eq-height']    Edit
    Element Should Contain    xpath=//div[@class='acc-main__block_addr row-eq-height']    Delete
    Element Should Contain    xpath=//div[@class='acc-main__block_addr row-eq-height']    Make Default
    Sleep    15s
    Scroll Element Into View    xpath=(//a[text()='Make Default'])
    Click Element    xpath=(//a[text()='Make Default'])
    Sleep    4s
    Click Element    xpath=//a[contains(text(),'Delete')]    #clicks on delete link
    Sleep    2s
    Click Element    xpath=(//button[@class='close'])[2]    #click on delete adress pop up close icon
    Sleep    2s
    Click Element    xpath=//div[@class='acc-main__block_addr row-eq-height']//a[contains(text(),'Delete')]    #clicks on delete link
    Sleep    2s
    Click Element    xpath=(//a[@class='btn btn-link'][contains(text(),'Cancel')])[2]    #click on link cancel button
    Sleep    2s
    Scroll Element Into View    xpath=//a[contains(text(),'Delete')]    #Click On Delete button.
    Click Element    xpath=//a[contains(text(),'Delete')]    #Click On Delete button.
    Sleep    2s
    Click Element    xpath=(//button[@class='btn btn-primary modal-btn'][contains(text(),'Delete')])[2]
    Sleep    4s
    Click Element    xpath=//a[contains(text(),'Edit')]
    Sleep    4s
    edit address
    Click Element    xpath=//input[@class='btn btn-primary']    #clicks on submit button
    Sleep    5s
    Click Element    xpath=//a[contains(text(),'Delete')]    #clicks 1st address delete link
    Sleep    2s
    Click Element    xpath=//button[@class='btn btn-primary modal-btn']    #clicks on delete button from the pop up
    Sleep    2s
    Element Should Contain    class=global-alerts    Your address was removed.

Bill Address details
    Click Element    id=billinginputSchoolName
    Input Text    id=billinginputSchoolName    @{Address}[16]
    Sleep    2s
    Click Element    id=billinginputFirstName
    Input Text    id=billinginputFirstName    @{Address}[17]
    Sleep    2s
    Click Element    id=billinginputLastName
    Input Text    id=billinginputLastName    @{Address}[26]
    Sleep    2s
    Click Element    id=billinginputAddress1
    Input Text    id=billinginputAddress1    @{Address}[27]
    Sleep    2s
    Click Element    id=billinginputCityName
    Input Text    id=billinginputCityName    @{Address}[28]
    Sleep    2s
    Click Element    id=js-region
    Select From List By Value    id=js-region    @{Address}[29]
    Sleep    2s
    Click Element    id=billinginputZIPCode
    Input Text    id=billinginputZIPCode    @{Address}[30]
    Sleep    2s
    Click Element    id=billinginputPhoneNumber
    Input Text    id=billinginputPhoneNumber    @{Address}[31]
    Sleep    2s

Bill Adress detail2
    Click Element    id=billinginputSchoolName
    Input Text    id=billinginputSchoolName    @{Address}[24]
    Sleep    2s
    Click Element    id=billinginputFirstName
    Input Text    id=billinginputFirstName    @{Address}[25]
    Sleep    2s
    Click Element    id=billinginputLastName
    Input Text    id=billinginputLastName    @{Address}[26]
    Sleep    2s
    Click Element    id=billinginputAddress1
    Input Text    id=billinginputAddress1    @{Address}[27]
    Sleep    2s
    Click Element    id=billinginputCityName
    Input Text    id=billinginputCityName    @{Address}[28]
    Sleep    2s
    Click Element    id=js-region
    Select From List By Value    id=js-region    @{Address}[29]
    Sleep    2s
    Click Element    id=billinginputZIPCode
    Input Text    id=billinginputZIPCode    @{Address}[30]
    Sleep    2s
    Click Element    id=billinginputPhoneNumber
    Input Text    id=billinginputPhoneNumber    @{Address}[31]
    Sleep    2s
    Element Text Should Be    xpath=//label[contains(text(),'Make Default')]    Make Default

Order detail page
    Click Element    xpath=//div[contains(@class,'yCmsComponent col-xs-5 col-sm-4 col-md-3 navbar-header__logo')]//img[@class='img-responsive']
    Sleep    10s
    Click Element    xpath=//a[@id='myaccount-popup']    #Click on my account pop up
    Sleep    2s
    Click Element    xpath=(//a[contains(text(),'Track Orders')])[1]    #Clicks on Track Orders
    Sleep    2s
    ${order}    Get Text    xpath=//a[@class='acc-main__block_table_tr-link']
    Input Text    xpath=//input[@id='js-order-search__input']    ${order}
    Sleep    2s
    Click Element    xpath=//div[contains(@class,'js-order-search')]//button[contains(@class,'btn btn-primary btn-search')]
    Sleep    2s
    Click Element    xpath=//a[@class='acc-main__block_table_tr-link']
    Sleep    2s
    Element Should Contain    xpath=//a[@class='text-primary js-incidentPopup']    Issue with this Order    #verifies issue with order CTA
    Sleep    2s
    #issue with order
    Sleep    2s
    #Close Incident
    Get Window Titles

issue with order
    Click Element    xpath=//a[@class='text-primary js-incidentPopup']
    Sleep    2s
    Page Should Contain Image    @{product Image}
    Sleep    2s
    ${name}    Get Text    xpath=//div[@class='details']
    Element Should Contain    xpath=//div[@id='colorbox']//form[@id='command']//div[1]    Select a reason from the list
    Sleep    2s
    Click Element    xpath=//select[@id='reasonSelector']    #clicks on dropdown
    Select From List By Value    xpath=//select[@id='reasonSelector']    2
    Select From List By Value    xpath=//select[@id='reasonSelector']    3
    Select From List By Value    xpath=//select[@id='reasonSelector']    4
    Select From List By Value    xpath=//select[@id='reasonSelector']    5
    Select From List By Value    xpath=//select[@id='reasonSelector']    1
    #Click Element    xpath=//span[@class='glyphicon glyphicon-remove']    #clicks on close icon
    Element Text Should Be    xpath=//label[contains(text(),'Please provide any additional information that mig')]    Please provide any additional information that might be helpful:
    Input Text    xpath=//textarea[@name='message']    Sample Text
    Element Should Contain    xpath=//button[@class='btn-warning btn btn-block']    Submit
    Click Element    xpath=//button[@class='btn-warning btn btn-block']
    Sleep    4s
    Element Should Contain    xpath=//div[@class='alert alert-warning alert-dismissable getAccAlert']    Your incident declaration has been opened successfully.
    Sleep    2s
    Element Text Should Be    xpath=//div[@class='checkout-summary__product_list col-sm-3 col-xs-12 order_line_status order_consignments_details hidden-xs']    INCIDENT OPEN

Close Incident
    Element Should Contain    xpath=//a[@class='text-default js-incidentPopup']    Close the incident
    Sleep    2s
    Click Element    xpath=//a[@class='text-default js-incidentPopup']
    Sleep    2s
    Click Element    xpath=//span[@class='glyphicon glyphicon-remove']    #clicks on close icon
    Sleep    2s
    Click Element    xpath=//a[@class='text-default js-incidentPopup']
    Page Should Contain Image    @{product Image}
    Sleep    2s
    ${name}    Get Text    xpath=//div[@class='details']
    Element Should Contain    xpath=//div[@id='colorbox']//form[@id='command']//div[1]    Select a reason from the list
    Sleep    2s
    Click Element    xpath=//select[@id='reasonSelector']    #clicks on dropdown
    Select From List By Value    xpath=//select[@id='reasonSelector']    9
    Select From List By Value    xpath=//select[@id='reasonSelector']    10
    Select From List By Value    xpath=//select[@id='reasonSelector']    11
    Select From List By Value    xpath=//select[@id='reasonSelector']    8
    Element Text Should Be    xpath=//label[contains(text(),'Please provide any additional information that mig')]    Please provide any additional information that might be helpful:
    Input Text    xpath=//textarea[@name='message']    Sample Text
    Sleep    2s
    Click Element    xpath=//button[@class='btn-warning btn btn-block']    #click on submit

Unauntheticated user
    #Element Should Be Visible    xpath=//*[@id="homepageHeroCarousel"]/div[1]/div/div[4]/div/div[1]/img
    Sleep    3s
    #Element Text Should Be    xpath=//div[contains(@class,'owl-item active')]//a[contains(@class,'btn cta-2 white-stroke')][contains(text(),'Shop Restaurant Products')]    Shop Restaurant Products    #verifies \ CTA on Hero banner
    Sleep    3s
    #Click Element    xpath=//div[contains(@class,'owl-item active')]//a[contains(@class,'btn cta-2 white-stroke')][contains(text(),'Shop Restaurant Products')]    #clicks on CTA button
    #Sleep    3s
    Element Should Be Visible    xpath=//img[@alt='Kitchen-Equipment.jpg']    #verifies components available on homepage.
    Element Should Be Visible    xpath=//img[@alt='Light-Counter-Equipment.jpg']    #verifies components available on homepage.
    Element Should Be Visible    xpath=//img[@alt='Bar-Equipment.jpg']    #verifies components available on homepage.
    Element Should Be Visible    xpath=//img[@alt='Table-Top.jpg']    #verifies components available on homepage.
    Element Should Be Visible    xpath=//img[@alt='Smallwares.jpg']    #verifies components available on homepage.
    Element Should Be Visible    xpath=//img[@alt='Furniture.jpg']    #verifies components available on homepage.
    Element Should Be Visible    xpath=//img[@alt='Lodging.jpg']    #verifies components available on homepage.
    Element Should Be Visible    xpath=//img[@alt='Janitorial.jpg']    #verifies components available on homepage.
    Sleep    2s
    ${width}    ${height}    Get Element Size    xpath=//*[@id="homepageHeroCarousel"]/div[1]/div/div[4]/div/div[1]/img

Authenticated user
    Element Should Be Visible    xpath=//*[@id="homepageHeroCarousel"]/div[1]/div/div[4]/div/div[1]/img
    Sleep    3s
    Element Text Should Be    xpath=//div[contains(@class,'owl-item active')]//a[contains(@class,'btn cta-2 white-stroke')][contains(text(),'Shop Restaurant Products')]    Shop Restaurant Products    #verifies \ CTA on Hero banner
    Sleep    3s
    Click Element    xpath=//div[contains(@class,'owl-item active')]//a[contains(@class,'btn cta-2 white-stroke')][contains(text(),'Shop Restaurant Products')]    #clicks on CTA button
    Sleep    3s
    Element Should Be Visible    xpath=//img[@alt='Kitchen-Equipment.jpg']    #verifies components available on homepage.
    Element Should Be Visible    xpath=//img[@alt='Light-Counter-Equipment.jpg']    #verifies components available on homepage.
    Element Should Be Visible    xpath=//img[@alt='Bar-Equipment.jpg']    #verifies components available on homepage.
    Element Should Be Visible    xpath=//img[@alt='Table-Top.jpg']    #verifies components available on homepage.
    Element Should Be Visible    xpath=//img[@alt='Smallwares.jpg']    #verifies components available on homepage.
    Element Should Be Visible    xpath=//img[@alt='Furniture.jpg']    #verifies components available on homepage.
    Element Should Be Visible    xpath=//img[@alt='Lodging.jpg']    #verifies components available on homepage.
    Element Should Be Visible    xpath=//img[@alt='Janitorial.jpg']    #verifies components available on homepage.
    Sleep    2s
    ${width}    ${height}    Get Element Size    xpath=//*[@id="homepageHeroCarousel"]/div[1]/div/div[4]/div/div[1]/img

Verify cart icon
    Element Should Be Visible    xpath=//div[@class='user-cart-navbar']
    Sleep    2s
    Click Element    id=signin-popup
    Click Element    xpath=//form[@id='loginajaxform']//input[@name='j_username']
    Input Text    xpath=//form[@id='loginajaxform']//input[@name='j_username']    @{EnterUsername}[2]
    Click Element    id=j_password
    Input Text    id=j_password    12341234
    Sleep    3s
    Click Element    id=loginsubmit
    Sleep    2s
    Click Element    xpath=//a[contains(text(),'Kitchen Equipment')]
    Element Should Be Visible    xpath=//div[@class='user-cart-navbar']    #verifies carticon on PLP
    Sleep    2s
    Click Element    xpath=//a[@id='myaccount-popup']
    Sleep    2s
    Click Element    xpath=//a[contains(text(),'Account Management')]
    Element Should Be Visible    xpath=//div[@class='user-cart-navbar']    #verifies carticon on myaccount page
    Kitchen PLP Compare
    Go To    https://accstorefront.c4f4c56-foodservi1-s1-public.model-t.cc.commerce.ondemand.com/en_US/sellers/2012
    Element Should Be Visible    xpath=//div[@class='user-cart-navbar']    #verifies carticon on myaccount page

Add 3 produucts to cart
    Click Element    xpath=//span[contains(@class,'icn-cart')]
    Sleep    2s
    Click Element    xpath=//div[6]//div[1]//div[2]//div[2]//a[1]    #Clicks on quick view
    Sleep    2s
    Click Element    xpath=/html[1]/body[1]/main[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[3]/div[10]/div[1]/div[1]/div[2]/form[1]/div[2]/button[1]
    Sleep    12s
    Click Element    xpath=//a[@class='quick-view__remove']
    Sleep    2s
    Click Element    xpath=//div[7]//div[1]//div[2]//div[2]//a[1]    #Clicks on quick view
    Sleep    2s
    #Click Element    xpath=//button[@id='addToCartButton']
    Sleep    12s
    Click Element    xpath=//a[@class='quick-view__remove']
    Sleep    2s
    Click Element    xpath=//div[8]//div[1]//div[2]//div[2]//a[1]    #Clicks on quick view
    Sleep    2s
    #Click Element    xpath=//button[@id='addToCartButton']
    Sleep    12s
    Click Element    xpath=//a[@class='quick-view__remove']
    Sleep    12s
    Click Element    xpath=//div[@class='user-cart-navbar']
    Sleep    2s

Cart page
    Click Element    xpath=//a[@class='btn btn-link btn-go2cart']    #navigaes to cart page
    Sleep    2s
    Element Text Should Be    xpath=//h1[@class='cart-page__header_title']    Shopping Cart
    Sleep    2s
    ${Seller}    Get Text    xpath=//div[contains(@class,'col-xs-12 col-md-8 col-lg-9 cart-page__inner')]
    Element Should Be Visible    xpath=//div[contains(@class,'yCmsComponent yComponentWrapper')]//div[1]//div[2]//div[1]//div[1]//a[1]//img[1]    #verifies product image
    Sleep    2s
    Element Should Be Visible    xpath=//input[@id='quantity_1']
    Sleep    2s
    Element Should Be Visible    xpath=(//input[@value='Update'])[1]
    Click Element    xpath=//input[@id='quantity_1']
    Input Text    xpath=//input[@id='quantity_1']    5
    Click Element    xpath=(//input[@value='Update'])[1]
    ${Sellerb}    Get Text    xpath=//div[contains(@class,'col-xs-12 col-md-8 col-lg-9 cart-page__inner')]
    Sleep    2s
    ${carttotal}    Get Text    xpath=//form[@id='cartEstimatedShippingForMirakl']
    Sleep    2s
    Click Element    xpath=//div[contains(@class,'yCmsComponent yComponentWrapper')]//div[1]//div[2]//div[1]//div[1]//a[1]//img[1]
    Go Back
    Element Should Contain    xpath=//div[contains(@class,'estimate-shipping cart-estimated-shipping')]    Estimate Shipping Cost
    Sleep    2s
    ${Cart total}    Get Text    xpath=//div[contains(@class,'cart-page__header_total')]
    Element Should Be Visible    xpath=//div[contains(@class,'cart-page__option')]//a[contains(@class,'btn btn-primary')][contains(text(),'Proceed to Checkout')]    #Verifies proceed to checkout button
    Element Should Be Visible    xpath=//a[contains(@class,'btn btn-link print-full-width')]    #verifies continue to shop button
    Sleep    2s
    Click Element    xpath=//a[contains(@class,'btn btn-link print-full-width')]
    Sleep    2s
    Click Element    xpath=//div[@class='user-cart-navbar']
    Sleep    2s
    Click Element    xpath=//a[@class='btn btn-link btn-go2cart']
    Sleep    2s
    Element Should Contain    xpath=//a[contains(text(),'Clear Cart')]    Clear Cart
    Sleep    5s
    Element Should Be Visible    xpath=//button[@id='cart-sub-estimated-shipping-btn']    #Verifies submit button
    Element Should Be Visible    xpath=//input[@id='cartEstimatedShippingCost']    #verifies zip field
    Input Text    xpath=//input[@id='cartEstimatedShippingCost']    10019
    Sleep    2s
    Click Element    xpath=//button[@id='cart-sub-estimated-shipping-btn']    #Click on submit button
    Sleep    10s
    ${shipping text}    Get Text    xpath=//div[@class='cart-page__summary_estimated_item row']    #xpath=//*[@id="cartEstimatedShippingForMirakl"]/div[2]
    Sleep    2s
    ${ship text}    Get Text    xpath=//span[@id='cart-estimated-shipping-display-value']
    Click Element    xpath=//input[@id='cartEstimatedShippingCost']
    Clear Element Text    xpath=//input[@id='cartEstimatedShippingCost']
    Click Element    xpath=//button[@id='cart-sub-estimated-shipping-btn']    #Click on submit button
    Element Text Should Be    xpath=//label[@class='error']    Please enter zip code
    ${currently not available}    Get Text    xpath=//div[@class='cart-page__summary_estimated_item row']
    Sleep    2s
    Input Text    xpath=//input[@id='cartEstimatedShippingCost']    1001    #verifies invalid zip code
    Click Element    xpath=//button[@id='cart-sub-estimated-shipping-btn']    #click on submit
    Element Text Should Be    xpath=//label[@class='error']    Please enter valid zip code    #Verifies error message.
    Click Element    xpath=//input[@id='cartEstimatedShippingCost']
    Recommended section
    Click Element    xpath=//a[contains(text(),'Clear Cart')]
    Sleep    2s
    Element Text Should Be    xpath=//h1[contains(@class,'cart-page__header_title')]    Your shopping cart is empty
    Sleep    2s
    Element Text Should Be    xpath=//h3[contains(@class,'cart-page__header_sub-title')]    Browse our products by clicking a category above from Navigation
    Sleep    2s
    Recommended section

Recommended section
    Element Should Be Visible    xpath=//div[@class='carousel__component']    #verifies recommended section visible or not
    Sleep    2s
    Element Should Be Visible    xpath=//div[@class='owl-stage']//div[1]//div[1]//a[1]//div[1]
    ${prod name}    Get Text    xpath=//div[@class='carousel__component']

Personal Information
    click element    xpath=//a[contains(text(),'Personal Information')]    #click on personal details link
    sleep    2s
    click element    xpath=//select[@id='text.myaccount.personal.details.prefix']    #click on prefix field in
    sleep    2s
    click element    xpath=//option[contains(text(),'none')]    #select none category from list
    sleep    2s
    click element    xpath=//option[contains(text(),'Dr.')]    #select Dr category
    sleep    2s
    click element    xpath=//option[contains(text(),'Miss')]    #select miss category
    sleep    2s
    click element    xpath=//option[contains(text(),'Rev.')]    #select rev
    click element    xpath=//option[contains(text(),'Mr.')]    #select Mr.
    sleep    2s
    click element    xpath=//option[contains(text(),'Mrs.')]    #select Mrs.
    click element    xpath=//option[contains(text(),'Ms.')]    #select Ms.
    Sleep    2s

Change Password
    click element    xpath=//a[contains(text(),'Change Password')]
    Input Text    xpath=//input[@id='currentPwd']    @{CurrentAndNewPWD}[0]    #input wrong old pwd in pwd field
    Input Text    xpath=//input[@id='newPwd']    @{CurrentAndNewPWD}[2]    #input new pwd in new pwd field
    Input Text    xpath=//input[@id='checkNewPwd']    @{CurrentAndNewPWD}[3]    #Input new pwd in cinfirm pwd field
    sleep    2s
    click element    xpath=//button[contains(@class,'btn btn-primary acc-btn')]    #Click update button
    sleep    2s
    ${Get Error msg}    Get Text    xpath=//div[contains(@class,'alert alert-danger alert-dismissable getAccAlert')]
    ${Get Errormsg}    Get Text    xpath=//span[@id='currentPassword.errors']
    click element    xpath=//a[contains(text(),'Cancel')]    #click on cancell button
    Go back
    Reload Page
    sleep    5s
    Input Text    xpath=//input[@id='currentPwd']    @{CurrentAndNewPWD}[1]    #input corrcet pwd in pwd filed
    Input Text    xpath=//input[@id='newPwd']    @{CurrentAndNewPWD}[2]    #input new pwd in new pwd field
    Input Text    xpath=//input[@id='checkNewPwd']    @{CurrentAndNewPWD}[3]    #Input new pwd in cinfirm pwd field
    click element    xpath=//button[contains(@class,'btn btn-primary acc-btn')]    #Click update button
